from enum import Enum
import random


class MutationEnum(Enum):
    SUPER_POWER = 3
    WEAK = 2
    GIANT = 2
    LILLIPUTIAN = 2
    HAIRLESS = 'Лысый'


class Mutation:
    def apply(self, character):
        raise NotImplemented

    def __str__(self):
        return self.__doc__


class SuperPower(Mutation):
    """Мутация утроения силы"""

    def apply(self, character):
        if character.strength is None:
            character.strength = MutationEnum.SUPER_POWER.value
        else:
            character.strength = character.strength * MutationEnum.SUPER_POWER.value


class Weak(Mutation):
    """Мутация удвоение слабости"""

    def apply(self, character):
        if character.strength is None:
            return
        else:
            character.strength = int(character.strength / MutationEnum.WEAK.value)


class Giant(Mutation):
    """Мутация удвоение роста"""

    def apply(self, character):
        if character.height is None:
            character.height = MutationEnum.GIANT.value
        else:
            character.race.height = character.height * MutationEnum.GIANT.value


class Lilliputian(Mutation):
    """Мутация уменьшения роста в два раза"""

    def apply(self, character):
        if character.height is None:
            character.height = MutationEnum.LILLIPUTIAN.value
        else:
            character.height = character.height / MutationEnum.LILLIPUTIAN.value


class Hairless(Mutation):
    """Стрижет налысо"""

    def apply(self, character):
        character.hair = MutationEnum.HAIRLESS.value


class MutationGenerator:
    mut_list = [SuperPower(), Weak(), Giant(), Lilliputian(), Hairless()]

    @staticmethod
    def get_list(random_range=3):
        return random.sample(MutationGenerator.mut_list, random.choice(range(0, random_range)))


class MutationIterator:
    """Итератор по списку"""

    def __init__(self, mutations: list):
        self.mutations = mutations
        self.current = 0
        self.end = len(mutations)

    def __iter__(self):
        return self

    def __next__(self):
        if self.current >= self.end:
            raise StopIteration
        result = self.mutations[self.current]
        self.current += 1
        return result
