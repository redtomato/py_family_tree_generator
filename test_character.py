import unittest
from main import Character
from races import RaceGenerator, Race


class TestCharacter(unittest.TestCase):

    def setUp(self) -> None:
        self.character = Character(RaceGenerator.get_race())

    def test_exists_gender(self):
        self.assertIsNotNone(self.character.gender)

    def test_race_instance(self):
        self.assertIsInstance(self.character.race, Race)
