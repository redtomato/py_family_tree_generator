from abc import ABCMeta, abstractmethod
import random
import json


class Race(metaclass=ABCMeta):
    """Базовый класс для расы"""

    def __init__(self):
        self.name = 'Noname'
        self.hands_number = None
        self.legs_number = None
        self.eyes_number = None
        self.skin_color = None

    def __str__(self):
        dictionary = {}
        #  print(vars(self))
        for k, v in vars(self).items():
            if k not in dictionary:
                dictionary[k] = None
            dictionary[k] = v
        return json.dumps(dictionary, ensure_ascii=False)


class HumanRace(Race):
    def __init__(self):
        super().__init__()
        self.name = 'Human'
        self.hands_number = 2
        self.legs_number = 2
        self.eyes_number = 2


class BugRace(Race):

    def __init__(self):
        super().__init__()
        self.name = 'Bug'
        self.legs_number = 6
        self.eyes_number = 2


class HobbitRace(Race):
    def __init__(self):
        super().__init__()
        self.name = 'Hobbit'
        self.legs_number = 2
        self.eyes_number = 2
        self.hands_number = 2


class RaceCreator(metaclass=ABCMeta):
    @abstractmethod
    def exec(self):
        pass


class HumanCreator(RaceCreator):
    """Класс для создание рассы людей"""

    def exec(self):
        return HumanRace()


class BugCreator(RaceCreator):
    """Класс для создание рассы жуков"""

    def exec(self):
        return BugRace()


class HobbitCreator(RaceCreator):
    """Класс для создание рассы жуков"""

    def exec(self):
        return HobbitRace()


class RaceGenerator:

    @staticmethod
    def get_race():
        races = (HumanCreator(), BugCreator(), HobbitCreator())
        return random.choice(races).exec()
