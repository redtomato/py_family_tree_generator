from mutation import MutationGenerator
from races import RaceGenerator
from character import Character
from tree_handler import TreeHandler


def create_parents(node, level):
    """ Функция создания родителей персонажа"""
    root = None
    return [create_tree(root, x.race, x.descendant, x.level, x.gender)
            for x in [Character(node.race, node, level, 'male'), Character(node.race, node, level, 'female')]]


def create_tree(node, race, descendant, level, gender=None):
    """ Функция создания дерева"""
    if node is None:
        node = Character(race, descendant, level, gender)
        node.mutations = MutationGenerator.get_list()
        node.height = level
        return node
    if level > node.level:
        nodes = TreeHandler.get_leafs(node)
        for n in nodes:
            n.parents = create_parents(n, level)
    node.height += level - node.height
    return node


def generate(deep=None):
    """Функция генерации дерева"""
    if deep is None:
        return None
    race = RaceGenerator.get_race()
    i = 0
    root = None
    while i <= deep:
        root = create_tree(root, race, root, i)
        i += 1
    return root


if __name__ == '__main__':
    ft = generate(4)
    print("Numbers of family members: {} ".format(TreeHandler.count_characters(ft)))
    parent_list_2 = TreeHandler.get_node(ft, 2)
    descendant_3 = TreeHandler.get_node_descendant(ft, 3)
    print(descendant_3.export())
    print('Mutation apply')
    descendant_3.mutate()
    print(descendant_3.export())
    print('------------------ tree -----------------------------')
    TreeHandler.draw(ft)
