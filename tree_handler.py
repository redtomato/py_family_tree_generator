class TreeHandler:
    @staticmethod
    def get_leafs(tree):
        """Метод поиска листьев (персонажей без родителей)"""
        leafs = []

        def get_leaf_nodes(node):
            if node is not None:
                if len(node.parents) == 0:
                    leafs.append(node)
                for n in node.parents:
                    get_leaf_nodes(n)

        get_leaf_nodes(tree)

        return leafs

    @staticmethod
    def create_parents_dictionary(tree):
        """Метод создания словаря персонажей в дереве"""
        dictionary = {}

        def get_dict(node):
            if node is not None:
                if node.level not in dictionary:
                    dictionary[node.level] = []
                dictionary[node.level].append(node)
                for n in node.parents:
                    get_dict(n)

        get_dict(tree)
        return dictionary

    @staticmethod
    def count_characters(tree):
        """Метод подсчета персонажей в дереве"""
        if tree is None:
            return 0
        count = 1
        if tree.parents:
            count += sum([TreeHandler.count_characters(parent) for parent in tree.parents])
        return count

    @staticmethod
    def get_node(tree, n):
        """ Метод получения родителей N-го колена"""
        d = TreeHandler.create_parents_dictionary(tree)
        try:
            return d[n]
        except KeyError:
            return None

    @staticmethod
    def get_node_descendant(tree, n):
        """ Метод получения потомка/ов N-го колена"""
        d = TreeHandler.create_parents_dictionary(tree)
        try:
            node = d[n]
            return node[0].descendant
        except KeyError:
            return None

    @staticmethod
    def draw(tree):
        """Функция отрисовки дерева в консоль"""

        def draw_nodes(node):
            if node is not None:
                lstr = "{}({})".format(node.name, node.gender)
                if node.descendant:
                    lstr += "parent of {}".format(node.descendant.name)
                print("\t" * node.level + lstr)
                for p in node.parents:
                    draw_nodes(p)

        draw_nodes(tree)