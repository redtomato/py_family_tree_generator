import json
from abc import ABCMeta
from mutation import MutationIterator


class CharacterExport(metaclass=ABCMeta):
    def export(self, character):
        pass


class ExportJson(CharacterExport):
    """ Класс экспорта сушности в формате json"""

    def export(self, character):
        if character.parents:
            parents = ", ".join([repr(parent) for parent in character.parents])

        return json.dumps({
            'name': character.name,
            'gender': character.gender,
            'race': str(character.race),
            'level': character.level,
            'mutation': ", ".join([str(mutation) for mutation in MutationIterator(character.mutations)])
            if character.mutations else "Нет",
            'descendant': repr(character.descendant),
            'parents': parents if character.parents else "Нет",
        }, ensure_ascii=False)
