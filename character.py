import random
from exports import ExportJson
from enum import Enum
from faker import Factory


class CharacterEnum(Enum):
    HAIR = ['Блондин', 'Лысый', 'Брюнет']


class Character:
    """Класс Персонажа"""
    names = {
        'female': ("Emma", "Olivia", "Ava",
                   "Sophia", "Charlotte", "Amelia",
                   "Isabella", "Lucas", "Mia", "Henry",
                   "Evelyn"),
        'male': ("Liam", "Noah", "Elijah",
                 "William", "James", "Benjamin",
                 "Henry", "Alexander", "Harper",)
    }

    def __init__(self, race, descendant=None, level=0, gender=None, exporter=None):
        self.gender = gender or random.choice(['female', 'male'])
        self.name = random.choice(Character.names[self.gender])
        self.race = race
        self.level = level
        self.descendant = descendant
        self._mutations = None
        self.parents = []
        self.height = 1
        self.strength = 1
        self.hair = random.choice(CharacterEnum.HAIR.value)
        faker = Factory().create()
        self.skin_color = random.choice([faker.color_name() for _ in range(10)])
        self._exporter = exporter or ExportJson()

    @property
    def mutations(self):
        return self._mutations

    @mutations.setter
    def mutations(self, mutations: list):
        self._mutations = mutations or None

    def mutate(self):
        """Метод применения мутаций к персонажу"""
        if self.mutations is None:
            return
        for mutation in self.mutations:
            mutation.apply(self)

    def export(self):
        """Метод выгрузки характеристи """
        return self._exporter.export(self)

    def __repr__(self):
        return f"Character('{self.race, self.descendant, self.level, self.gender}')"
